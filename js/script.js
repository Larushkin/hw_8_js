'use strict'

/**
 * 1.  Опишіть своїми словами що таке Document Object Model (DOM)
 *      Это независящий программный интерфейс, позволяющий получить доступ к содержимому HTML,
 *      а также позволяет изменять содержимое, структуру таких документов.
 *
 * 2.    Яка різниця між властивостями HTML-елементів innerHTML та innerText?
 *      innerHTML содержит всю html-структуру элемента : тэги, свойства, стили и тд.
 *      innerText содержит только  контент элемента.
 *
 * 3.   Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
 *      Для обращения  к html-обьекту существуют встроенные способы document:
 *      getElementById, getElementsByClass, getElementsByTagName, querySelector, querySelectorAll.
 *      с точки зрения уневирсальности и удобоств это: querySelector, querySelectorAll.

 * Завдання
 * Код для завдань лежить в папці project.
 *
 * 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
 * 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
 * 3. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
 * 4. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
 * 5. Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль.
 * 6. Кожному з елементів присвоїти новий клас nav-item.
 * 7. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.*
 */

//1
for (let item of document.querySelectorAll('p')) {
    item.style.backgroundColor = '#ff0000';
}

//2
console.log(document.querySelector('#optionsList'));
console.log(document.querySelector('#optionsList').parentElement);

//3

console.log(document.querySelector('#optionsList').childNodes);


//4
document.getElementById('testParagraph').innerText = 'This is paragraph';

//5, 6
for (let item of document.querySelector('.main-header').children) {
    console.log(item);
    item.classList.add('nav-item');
}

//7
for (let item of document.querySelectorAll('.section-title')) {
    item.classList.remove('section-title');
}
